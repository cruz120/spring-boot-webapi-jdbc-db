package com.example.springbootapijdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApiJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootApiJdbcApplication.class, args);
    }

}
